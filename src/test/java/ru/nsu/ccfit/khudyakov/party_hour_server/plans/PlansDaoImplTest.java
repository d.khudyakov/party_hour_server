package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.DayNumber;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PlansDaoImplTest {

    @Autowired
    PlansDao plansDao;

    @Test
    @Transactional
    @Rollback
    void addUnitPlan() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);

            UnitPlanTO unitPlanTO = new UnitPlanTO(
                    "Театр",
                    "Гроза",
                    3,
                    simpleDateFormat.parse("2020-07-01"));

            Assertions.assertThatCode(() -> plansDao.addUnitPlan(
                    unitPlanTO,
                    UUID.fromString("e16834e1-1217-4541-a1ea-9ee84ca4d7ae"))).doesNotThrowAnyException();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @Test
    @Transactional
    @Rollback
    void addRegularPlan() {
        RegularPlanTO regularPlanTO = new RegularPlanTO(
                "Футбол",
                "Играю с друзьями",
                3,
                Arrays.asList(new DayNumber(1), new DayNumber(5)));

        Assertions.assertThatCode(() -> plansDao.addRegularPlan(
                regularPlanTO,
                UUID.fromString("e16834e1-1217-4541-a1ea-9ee84ca4d7ae"))).doesNotThrowAnyException();
    }

    @Test
    @Transactional
    @Rollback
    void addUnitPlanTestRandomUserID() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);

            UnitPlanTO unitPlanTO = new UnitPlanTO(
                    "Театр",
                    "Гроза",
                    3,
                    simpleDateFormat.parse("2020-07-01"));

            Throwable thrown = assertThrows(DataAccessException.class, () -> plansDao.addUnitPlan(
                    unitPlanTO,
                    UUID.randomUUID()));

            assertNotNull(thrown.getMessage());

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Transactional
    @Rollback
    void addRegularPlanTestBadUserID() {
        RegularPlanTO regularPlanTO = new RegularPlanTO(
                "Футбол",
                "Играю с друзьями",
                3,
                Arrays.asList(new DayNumber(1), new DayNumber(5)));

        Throwable thrown = assertThrows(DataAccessException.class, () -> plansDao.addRegularPlan(regularPlanTO, null));
        assertNotNull(thrown.getMessage());
    }

    @Test
    @Transactional
    @Rollback
    void getRegularPlans() {
        List<RegularPlanTO> regularPlanTOList = plansDao.getRegularPlans(UUID.randomUUID());
        assertEquals(0, regularPlanTOList.size());
    }

    @Test
    @Transactional
    @Rollback
    void getUnitPlans() {
        List<UnitPlanTO> unitPlanTOList = plansDao.getUnitPlans(UUID.randomUUID());
        assertEquals(0, unitPlanTOList.size());
    }
}