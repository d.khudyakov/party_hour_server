package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.validation.BindingResult;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyFieldsException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserNameTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LoginControllerTest {

    @MockBean
    LoginService loginService;

    @Autowired
    LoginController loginController;

    @Test
    void testSignUpBadUser() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        UserTO user = BDDMockito.mock(UserTO.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);

        Throwable thrown = assertThrows(EmptyFieldsException.class, () ->
                loginController.signUp(user, bindingResult));

        assertNotNull(thrown.getMessage());
    }

    @Test
    void testSignInBadUserName() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        UserNameTO user = BDDMockito.mock(UserNameTO.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);

        Throwable thrown = assertThrows(EmptyFieldsException.class, () ->
                loginController.signIn(user, bindingResult));

        assertNotNull(thrown.getMessage());
    }

    @Test
    void testSignIn() {
        UserNameTO userName = new UserNameTO("billy");

        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(false);

        UserTO testUser = new UserTO();
        testUser.setUserName("billy");
        BDDMockito.given(loginService.getUserData(userName)).willReturn(testUser);

        UserTO user = loginController.signIn(userName, bindingResult);
        assertEquals(user.getUserName(), userName.getUserName());
    }

}