package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.validation.BindingResult;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyFieldsException;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyHeadersException;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class PlansControllerTest {

    @MockBean
    PlansService plansService;

    @Autowired
    PlansController plansController;

    @Test
    void addUnitPlanTestHeader() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        UnitPlanTO unitPlanTO = BDDMockito.mock(UnitPlanTO.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);

        Throwable thrown = assertThrows(EmptyFieldsException.class, () ->
                plansController.addUnitPlan(unitPlanTO, UUID.randomUUID(), bindingResult));

        assertNotNull(thrown.getMessage());
    }


    @Test
    void addUnitPlanTestFields() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        UnitPlanTO unitPlanTO = BDDMockito.mock(UnitPlanTO.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);

        Throwable thrown = assertThrows(EmptyHeadersException.class, () ->
                plansController.addUnitPlan(unitPlanTO, null, bindingResult));

        assertNotNull(thrown.getMessage());
    }

    @Test
    void addUnitPlan() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(false);
        UnitPlanTO unitPlanTO = BDDMockito.mock(UnitPlanTO.class);

        Assertions.assertThatCode(() -> plansController.addUnitPlan(unitPlanTO, UUID.randomUUID(), bindingResult))
                .doesNotThrowAnyException();
    }


    @Test
    void addNRegularPlanTestHeader() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        RegularPlanTO regularPlanTO = BDDMockito.mock(RegularPlanTO.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);

        Throwable thrown = assertThrows(EmptyHeadersException.class, () ->
                plansController.addRegularPlan(regularPlanTO, null, bindingResult));

        assertNotNull(thrown.getMessage());
    }

    @Test
    void addNRegularPlanTestFields() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        RegularPlanTO regularPlanTO = BDDMockito.mock(RegularPlanTO.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);

        Throwable thrown = assertThrows(EmptyFieldsException.class, () ->
                plansController.addRegularPlan(regularPlanTO, UUID.randomUUID(), bindingResult));

        assertNotNull(thrown.getMessage());
    }

    @Test
    void addRegularPlan() {
        BindingResult bindingResult = BDDMockito.mock(BindingResult.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(false);
        RegularPlanTO regularPlanTO = BDDMockito.mock(RegularPlanTO.class);

        Assertions.assertThatCode(() -> plansController.addRegularPlan(regularPlanTO, UUID.randomUUID(), bindingResult))
                .doesNotThrowAnyException();
    }

    @Test
    void getUnitPlans() {
        Assertions.assertThatCode(() -> plansController.getUnitPlans(UUID.randomUUID())).doesNotThrowAnyException();
    }

    @Test
    void getRegularPlans() {
        Assertions.assertThatCode(() -> plansController.getRegularPlans(UUID.randomUUID())).doesNotThrowAnyException();
    }
}