package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.UserNotFoundException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserNameTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@RunWith(SpringRunner.class)
class LoginServiceTest {

    @MockBean
    private LoginDao loginDao;

    @Autowired
    private LoginService loginService;

    @Test
    void testDuplicateUserName() {
        UserTO user = new UserTO("billy", "Billy", "Anderson", "");

        Assertions.assertThatCode(() -> loginService.addNewUser(user)).doesNotThrowAnyException();
    }

    @Test
    void testNotFoundUser() {
        String name = "billy";
        BDDMockito.given(loginDao.getUserData(name)).willReturn(Optional.empty());

        Throwable thrown = assertThrows(UserNotFoundException.class, () ->
                loginService.getUserData(new UserNameTO(name)));

        assertNotNull(thrown.getMessage());
    }

}