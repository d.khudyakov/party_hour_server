package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.LoginDao;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PlansServiceTest {

    @MockBean
    private PlansDao plansDao;

    @Autowired
    private PlansService plansService;

    @Test
    void addUnitPlan() {
        UnitPlanTO unitPlanTO = BDDMockito.mock(UnitPlanTO.class);
        Assertions.assertThatCode(() -> plansService.addUnitPlan(unitPlanTO, UUID.randomUUID())).doesNotThrowAnyException();
    }

    @Test
    void addRegularPlan() {
        RegularPlanTO regularPlanTO = BDDMockito.mock(RegularPlanTO.class);
        Assertions.assertThatCode(() -> plansService.addRegularPlan(regularPlanTO, UUID.randomUUID())).doesNotThrowAnyException();
    }

    @Test
    void getRegularPlans() {
        Assertions.assertThatCode(() -> plansService.getRegularPlans(UUID.randomUUID())).doesNotThrowAnyException();
    }

    @Test
    void getUnitPlans() {
        Assertions.assertThatCode(() -> plansService.getUnitPlans(UUID.randomUUID())).doesNotThrowAnyException();
    }
}