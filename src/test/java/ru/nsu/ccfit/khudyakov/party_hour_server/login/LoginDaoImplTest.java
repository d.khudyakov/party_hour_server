package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.NameAlreadyTakenException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class LoginDaoImplTest {

    LoginDaoImplTest() {
    }

    @Autowired
    LoginDao loginDao;

    @Test
    @Transactional
    @Rollback
    public void testDuplicateUserName() {
        UserTO firstUser = new UserTO("billy1", "Billy", "Anderson", "");
        UserTO secondUser = new UserTO("billy1", "Billy", "Donner", "");

        loginDao.addNewUser(firstUser);

        Throwable thrown = assertThrows(NameAlreadyTakenException.class, () -> loginDao.addNewUser(secondUser));

        assertNotNull(thrown.getMessage());
    }

    @Test
    @Transactional
    @Rollback
    public void testEmptyGetUserData() {
        Optional<UserTO> optionalUserTO = loginDao.getUserData("");
        assertFalse(optionalUserTO.isPresent());
    }

    @Test
    @Transactional
    @Rollback
    public void testGetUserData() {
        UserTO testUser = new UserTO("billy", "Billy", "Anderson", "");

        loginDao.addNewUser(testUser);
        Optional<UserTO> optionalUserTO = loginDao.getUserData("billy");
        UserTO user = optionalUserTO.orElseThrow(AssertionError::new);

        assertEquals(testUser, user);
    }
}