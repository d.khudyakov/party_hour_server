package ru.nsu.ccfit.khudyakov.party_hour_server.errors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyFieldsException;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyHeadersException;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.UnknownStateError;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.NameAlreadyTakenException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.UserNotFoundException;

import static org.junit.Assert.*;

@SpringBootTest
class ErrorControllerTest {

    @Autowired
    ErrorController errorController;

    @Test
    void processNameException() {
        ErrorMessage errorMessage = errorController.processNameException(new NameAlreadyTakenException("Bob"));
        assertEquals(errorMessage, new ErrorMessage(ErrorController.getNameAlreadyTaken(), "Name Bob is already taken"));
    }

    @Test
    void processUserNotFoundException() {
        ErrorMessage errorMessage = errorController.processUserNotFoundException(new UserNotFoundException("Bob"));
        assertEquals(errorMessage, new ErrorMessage(ErrorController.getUserNotFound(), "User named Bob doesn't exist"));
    }

    @Test
    void processUnknownStateException() {
        ErrorMessage errorMessage = errorController.processUnknownStateException(new UnknownStateError());
        assertEquals(errorMessage, new ErrorMessage(ErrorController.getUnknownError(), "Unknown error"));
    }

    @Test
    void processEmptyFieldsException() {
        ErrorMessage errorMessage = errorController.processEmptyFieldsException(new EmptyFieldsException(""));
        assertEquals(errorMessage, new ErrorMessage(ErrorController.getEmptyFields(), ""));
    }

    @Test
    void processEmptyHeadersException() {
        ErrorMessage errorMessage = errorController.processEmptyHeadersException(new EmptyHeadersException(""));
        assertEquals(errorMessage, new ErrorMessage(ErrorController.getEmptyHeaders(), ""));
    }
}