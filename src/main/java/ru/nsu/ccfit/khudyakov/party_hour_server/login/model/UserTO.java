package ru.nsu.ccfit.khudyakov.party_hour_server.login.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Objects;

public class UserTO {
    @Null(groups = {NewUser.class})
    private String userID;

    @NotEmpty(groups = {NewUser.class})
    private String userName;

    @NotEmpty(groups = {NewUser.class})
    private String firstName;

    @NotEmpty(groups = {NewUser.class})
    private String secondName;

    @NotNull(groups = {NewUser.class})
    private String pictureURL;

    public UserTO() {

    }


    public UserTO(String userID, String userName, String firstName, String secondName, String pictureURL) {
        this.userID = userID;
        this.userName = userName;
        this.firstName = firstName;
        this.secondName = secondName;
        this.pictureURL = pictureURL;
    }

    public UserTO(String userName, String firstName, String secondName, String pictureURL) {
        this.userName = userName;
        this.firstName = firstName;
        this.secondName = secondName;
        this.pictureURL = pictureURL;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserTO userTO = (UserTO) o;
        return Objects.equals(userName, userTO.userName) &&
                Objects.equals(firstName, userTO.firstName) &&
                Objects.equals(secondName, userTO.secondName) &&
                Objects.equals(pictureURL, userTO.pictureURL);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, firstName, secondName, pictureURL);
    }
}
