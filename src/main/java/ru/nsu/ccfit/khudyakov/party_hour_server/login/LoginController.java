package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyFieldsException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.NewUser;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserNameTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

import javax.validation.Valid;

@RestController
public class LoginController {
    private static final String PATH = "api/v001/partyhour/";

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public String hello() {
        return "Hello! It is REST service for party hour project!";
    }

    @PostMapping(PATH + "signup")
    @ResponseStatus(HttpStatus.CREATED)
    public void signUp(@Validated(NewUser.class) @RequestBody UserTO userTO,
                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new EmptyFieldsException("Not all fields of the registration form were completed");
        } else {
            loginService.addNewUser(userTO);
        }
    }

    @PostMapping(PATH + "signin")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserTO signIn(@Valid @RequestBody UserNameTO userNameTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new EmptyFieldsException("Username is empty");
        } else {
            return loginService.getUserData(userNameTO);
        }
    }
}
