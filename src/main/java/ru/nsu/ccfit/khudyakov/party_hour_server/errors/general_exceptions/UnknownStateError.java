package ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions;

public class UnknownStateError extends RuntimeException {
    public UnknownStateError() {
        super("Unknown error");
    }
}
