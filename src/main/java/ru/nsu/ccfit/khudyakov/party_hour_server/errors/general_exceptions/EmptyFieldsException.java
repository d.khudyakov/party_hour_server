package ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions;

public class EmptyFieldsException extends RuntimeException {
    public EmptyFieldsException(String message) {
        super(message);
    }
}
