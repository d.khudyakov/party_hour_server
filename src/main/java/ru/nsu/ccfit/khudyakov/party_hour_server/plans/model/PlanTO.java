package ru.nsu.ccfit.khudyakov.party_hour_server.plans.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class PlanTO {
    @Null(groups = {NewPlan.class})
    private Integer planID;

    @NotEmpty(groups = {NewPlan.class})
    private String title;

    @NotNull(groups = {NewPlan.class})
    private String description;

    @NotNull(groups = {NewPlan.class})
    private Integer relevance;

    PlanTO() {
    }

    PlanTO(int planID, String title, String description, int relevance) {
        this.planID = planID;
        this.title = title;
        this.description = description;
        this.relevance = relevance;
    }

    PlanTO(String title, String description, int relevance) {
        this.title = title;
        this.description = description;
        this.relevance = relevance;
    }

    public void setPlanID(Integer planID) {
        this.planID = planID;
    }

    public void setRelevance(Integer relevance) {
        this.relevance = relevance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRelevance() {
        return relevance;
    }

    public void setRelevance(int relevance) {
        this.relevance = relevance;
    }
}
