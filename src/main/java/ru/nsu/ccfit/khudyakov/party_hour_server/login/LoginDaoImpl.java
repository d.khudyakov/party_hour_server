package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.NameAlreadyTakenException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

import java.util.Optional;
import java.util.UUID;

@Component
public class LoginDaoImpl implements LoginDao {
    private static final String SQL_GET_USER_ID_BY_LOGIN =
            "SELECT * FROM \"Users\" WHERE \"user_name\" = :user_name";

    private static final String SQL_INSERT_USER =
            "INSERT INTO \"Users\" VALUES(:user_id, :user_name, :first_name, :second_name, :picture_url)";

    private static final String USER_ID_PARAM = "user_id";
    private static final String USER_NAME_PARAM = "user_name";
    private static final String FIRST_NAME_PARAM = "first_name";
    private static final String SECOND_NAME_PARAM = "second_name";
    private static final String PICTURE_URL_PARAM = "picture_url";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public LoginDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private MapSqlParameterSource buildUserParameters(UserTO userTO) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource
                .addValue(USER_ID_PARAM, UUID.randomUUID())
                .addValue(USER_NAME_PARAM, userTO.getUserName())
                .addValue(FIRST_NAME_PARAM, userTO.getFirstName())
                .addValue(SECOND_NAME_PARAM, userTO.getSecondName())
                .addValue(PICTURE_URL_PARAM, userTO.getPictureURL());
        return parameterSource;
    }

    @Override
    public void addNewUser(UserTO userTO) {
        MapSqlParameterSource params = buildUserParameters(userTO);
        try {
            jdbcTemplate.update(SQL_INSERT_USER, params);
        } catch (DataAccessException e) {
            throw new NameAlreadyTakenException(userTO.getUserName());
        }
    }

    @Override
    public Optional<UserTO> getUserData(String userName) {
        try {
            return Optional.ofNullable(
                    jdbcTemplate.queryForObject(
                            SQL_GET_USER_ID_BY_LOGIN,
                            new MapSqlParameterSource(USER_NAME_PARAM, userName),
                            (resultSet, i) -> new UserTO(
                                    resultSet.getString(USER_ID_PARAM),
                                    resultSet.getString(USER_NAME_PARAM),
                                    resultSet.getString(FIRST_NAME_PARAM),
                                    resultSet.getString(SECOND_NAME_PARAM),
                                    resultSet.getString(PICTURE_URL_PARAM))
                    )
            );
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
