package ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String userName) {
        super("User named " + userName + " doesn't exist");
    }
}
