package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.UserNotFoundException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserNameTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

@Component
public class LoginServiceImpl implements LoginService {

    private final LoginDao loginDaoImpl;

    @Autowired
    public LoginServiceImpl(LoginDao loginDaoImpl) {
        this.loginDaoImpl = loginDaoImpl;
    }

    @Override
    public void addNewUser(UserTO userTO) {
        loginDaoImpl.addNewUser(userTO);
    }

    @Override
    public UserTO getUserData(UserNameTO userNameTO) {
        return loginDaoImpl.getUserData(userNameTO.getUserName())
                .orElseThrow(() -> new UserNotFoundException(userNameTO.getUserName()));
    }
}
