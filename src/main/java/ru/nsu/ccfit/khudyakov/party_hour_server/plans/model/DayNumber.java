package ru.nsu.ccfit.khudyakov.party_hour_server.plans.model;

public class DayNumber {
    private int number;

    public DayNumber() {

    }

    public DayNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
