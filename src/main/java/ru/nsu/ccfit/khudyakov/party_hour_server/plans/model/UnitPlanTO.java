package ru.nsu.ccfit.khudyakov.party_hour_server.plans.model;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class UnitPlanTO extends PlanTO {

    @NotNull(groups = {NewPlan.class})
    private Date date;

    public UnitPlanTO() {
    }

    public UnitPlanTO(int planID, String title, String description, int relevance, Date date) {
        super(planID, title, description, relevance);
        this.date = date;
    }

    public UnitPlanTO(String title, String description, int relevance, Date date) {
        super(title, description, relevance);
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
