package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyFieldsException;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyHeadersException;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.NewPlan;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import java.util.List;
import java.util.UUID;

@RestController
public class PlansController {
    private static final String PATH = "api/v001/partyhour/";
    private static final String USER_ID_ERROR_MSG = "UserID is not set";

    private final PlansService plansService;

    @Autowired
    public PlansController(PlansService plansService) {
        this.plansService = plansService;
    }

    @PostMapping(PATH + "unitplans")
    @ResponseStatus(HttpStatus.CREATED)
    public void addUnitPlan(@Validated(NewPlan.class) @RequestBody UnitPlanTO unitPlan,
                            @RequestHeader UUID userID,
                            BindingResult bindingResult) {
        if (userID == null) {
            throw new EmptyHeadersException(USER_ID_ERROR_MSG);
        } else if (bindingResult.hasErrors()) {
            throw new EmptyFieldsException("Not all required fields of the unit plan adding form are filled");
        } else {
            plansService.addUnitPlan(unitPlan, userID);
        }
    }

    @PostMapping(PATH + "regplans")
    @ResponseStatus(HttpStatus.CREATED)
    public void addRegularPlan(@Validated(NewPlan.class) @RequestBody RegularPlanTO regularPlan,
                               @RequestHeader UUID userID,
                               BindingResult bindingResult) {
        if (userID == null) {
            throw new EmptyHeadersException(USER_ID_ERROR_MSG);
        } else if (bindingResult.hasErrors()) {
            throw new EmptyFieldsException("Not all required fields of the regular plan adding form are filled");
        } else {
            plansService.addRegularPlan(regularPlan, userID);
        }
    }

    @GetMapping(PATH + "unitplans")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UnitPlanTO> getUnitPlans(@RequestHeader UUID userID) {
        if (userID == null) {
            throw new EmptyHeadersException(USER_ID_ERROR_MSG);
        } else {
            return plansService.getUnitPlans(userID);
        }
    }

    @GetMapping(PATH + "regplans")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<RegularPlanTO> getRegularPlans(@RequestHeader UUID userID) {
        if (userID == null) {
            throw new EmptyHeadersException(USER_ID_ERROR_MSG);
        } else {
            return plansService.getRegularPlans(userID);
        }
    }

}