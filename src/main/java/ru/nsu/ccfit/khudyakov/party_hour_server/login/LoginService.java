package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserNameTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

@Service
public interface LoginService {
    void addNewUser(UserTO userTO);

    UserTO getUserData(UserNameTO userNameTO);
}
