package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import java.util.List;
import java.util.UUID;

@Service
public class PlansServiceImpl implements PlansService {
    private final PlansDao plansDao;

    @Autowired
    public PlansServiceImpl(PlansDao plansDao) {
        this.plansDao = plansDao;
    }

    @Override
    public void addUnitPlan(UnitPlanTO unitPlan, UUID userID) {
        plansDao.addUnitPlan(unitPlan, userID);
    }

    @Override
    public void addRegularPlan(RegularPlanTO regularPlan, UUID userID) {
        plansDao.addRegularPlan(regularPlan, userID);
    }

    @Override
    public List<RegularPlanTO> getRegularPlans(UUID userID) {
        return plansDao.getRegularPlans(userID);
    }

    @Override
    public List<UnitPlanTO> getUnitPlans(UUID userID) {
        return plansDao.getUnitPlans(userID);
    }
}
