package ru.nsu.ccfit.khudyakov.party_hour_server.login.model;

import javax.validation.constraints.NotEmpty;

public class UserNameTO {
    @NotEmpty(message = "Please provide a username")
    private String userName;

    public UserNameTO() {

    }

    public UserNameTO(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
