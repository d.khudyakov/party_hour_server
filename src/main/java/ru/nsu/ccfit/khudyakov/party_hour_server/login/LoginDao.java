package ru.nsu.ccfit.khudyakov.party_hour_server.login;

import ru.nsu.ccfit.khudyakov.party_hour_server.login.model.UserTO;

import java.util.Optional;

public interface LoginDao {
    void addNewUser(UserTO userTO);

    Optional<UserTO> getUserData(String userName);
}
