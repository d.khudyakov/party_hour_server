package ru.nsu.ccfit.khudyakov.party_hour_server.plans.model;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class RegularPlanTO extends PlanTO {

    @NotEmpty(groups = {NewPlan.class})
    private List<DayNumber> days;

    public RegularPlanTO() {
    }

    public RegularPlanTO(int planID, String title, String description, int relevance, List<DayNumber> days) {
        super(planID, title, description, relevance);
        this.days = days;
    }

    public RegularPlanTO(String title, String description, int relevance, List<DayNumber> days) {
        super(title, description, relevance);
        this.days = days;
    }

    public List<DayNumber> getDays() {
        return days;
    }

    public void setDays(List<DayNumber> days) {
        this.days = days;
    }

    public List<Integer> convertDays() {
        List<Integer> ints = new ArrayList<>();

        for (DayNumber dayNumber : days) {
            ints.add(dayNumber.getNumber());
        }

        return ints;
    }
}
