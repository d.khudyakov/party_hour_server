package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.UnknownStateError;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.DayNumber;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.PlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import javax.sql.DataSource;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class PlansDaoImpl implements PlansDao {

    private static final String SQL_INSERT_PLAN =
            "INSERT INTO \"Plans\" (\"user_id\", \"title\", \"description\", \"relevance\") " +
                    "VALUES(:user_id, :title, :description, :relevance) RETURNING \"plan_id\"";

    private static final String SQL_INSERT_REGULAR_PLAN =
            "INSERT INTO \"RegularPlans\" VALUES(:plan_id, :day_numbers)";

    private static final String SQL_INSERT_UNIT_PLAN =
            "INSERT INTO \"UnitPlans\" VALUES(:plan_id, :date)";

    private static final String SQL_GET_UNIT_PLANS =
            " SELECT * FROM \"UnitPlans\" INNER JOIN \"Plans\" USING(\"plan_id\")  WHERE \"user_id\" = :user_id";

    private static final String SQL_GET_REGULAR_PLANS =
            " SELECT * FROM \"RegularPlans\" INNER JOIN \"Plans\" USING(\"plan_id\")  WHERE \"user_id\" = :user_id";

    private static final String USER_ID_PARAM = "user_id";
    private static final String PLAN_ID_PARAM = "plan_id";
    private static final String TITLE_PARAM = "title";
    private static final String DESCRIPTION_PARAM = "description";
    private static final String RELEVANCE_PARAM = "relevance";
    private static final String DATE_PARAM = "date";
    private static final String DAY_NUMBERS_PARAM = "day_numbers";


    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSource dataSource;

    @Autowired
    public PlansDaoImpl(NamedParameterJdbcTemplate jdbcTemplate, DataSource dataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;
    }

    private MapSqlParameterSource buildPlanParameters(PlanTO planTO, UUID userID) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(USER_ID_PARAM, userID);
        parameterSource.addValue(TITLE_PARAM, planTO.getTitle());
        parameterSource.addValue(DESCRIPTION_PARAM, planTO.getDescription());
        parameterSource.addValue(RELEVANCE_PARAM, planTO.getRelevance());

        return parameterSource;
    }

    private Optional<Integer> addPlan(MapSqlParameterSource planParams) {
        Optional<Integer> planId;

        try {
            planId = Optional.ofNullable(jdbcTemplate.queryForObject(
                    SQL_INSERT_PLAN,
                    planParams,
                    (resultSet, i) -> resultSet.getInt(PLAN_ID_PARAM)));
        } catch (EmptyResultDataAccessException e) {
            planId = Optional.empty();
        }

        return planId;
    }

    private MapSqlParameterSource buildUnitPlanParameters(UnitPlanTO unitPlanTO, int planID) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(PLAN_ID_PARAM, planID);
        parameterSource.addValue(DATE_PARAM, unitPlanTO.getDate());
        return parameterSource;
    }

    @Override
    public void addUnitPlan(UnitPlanTO unitPlan, UUID userID) {
        MapSqlParameterSource planParams = buildPlanParameters(unitPlan, userID);

        MapSqlParameterSource unitPlanParams = buildUnitPlanParameters(
                unitPlan,
                addPlan(planParams).orElseThrow(UnknownStateError::new));

        jdbcTemplate.update(SQL_INSERT_UNIT_PLAN, unitPlanParams);
    }

    private MapSqlParameterSource buildRegularPlanParameters(RegularPlanTO regularPlan, int planID) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue(PLAN_ID_PARAM, planID);

        try (Connection connection = dataSource.getConnection()) {
            Array array = connection.createArrayOf("integer", regularPlan.convertDays().toArray());
            parameterSource.addValue(DAY_NUMBERS_PARAM, array);
        } catch (SQLException e) {
            throw new UnknownStateError();
        }
        return parameterSource;
    }

    @Override
    public void addRegularPlan(RegularPlanTO regularPlan, UUID userID) {
        MapSqlParameterSource planParams = buildPlanParameters(regularPlan, userID);

        MapSqlParameterSource regularPlanParams = buildRegularPlanParameters(
                regularPlan,
                addPlan(planParams).orElseThrow(UnknownStateError::new));

        jdbcTemplate.update(SQL_INSERT_REGULAR_PLAN, regularPlanParams);
    }


    @Override
    public List<RegularPlanTO> getRegularPlans(UUID userID) {
        return jdbcTemplate.query(
                SQL_GET_REGULAR_PLANS,
                new MapSqlParameterSource(USER_ID_PARAM, userID),
                (resultSet, i) -> {
                    Integer[] numbers = (Integer[]) resultSet.getArray(DAY_NUMBERS_PARAM).getArray();

                    List<DayNumber> dayNumbers = new ArrayList<>();
                    for (Integer number : numbers) {
                        dayNumbers.add(new DayNumber(number));
                    }

                    return new RegularPlanTO(
                            resultSet.getInt(PLAN_ID_PARAM),
                            resultSet.getString(TITLE_PARAM),
                            resultSet.getString(DESCRIPTION_PARAM),
                            resultSet.getInt(RELEVANCE_PARAM),
                            dayNumbers
                    );
                });
    }

    @Override
    public List<UnitPlanTO> getUnitPlans(UUID userID) {
        return jdbcTemplate.query(
                SQL_GET_UNIT_PLANS,
                new MapSqlParameterSource(USER_ID_PARAM, userID),
                (resultSet, i) ->
                        new UnitPlanTO(
                                resultSet.getInt(PLAN_ID_PARAM),
                                resultSet.getString(TITLE_PARAM),
                                resultSet.getString(DESCRIPTION_PARAM),
                                resultSet.getInt(RELEVANCE_PARAM),
                                resultSet.getDate(DATE_PARAM)
                        ));
    }
}
