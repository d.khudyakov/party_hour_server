package ru.nsu.ccfit.khudyakov.party_hour_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ru.nsu.ccfit.khudyakov.party_hour_server.db_config.ConnectionSettings;

@SpringBootApplication
@EnableConfigurationProperties(ConnectionSettings.class)
public class PartyhourServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PartyhourServerApplication.class, args);
    }

}
