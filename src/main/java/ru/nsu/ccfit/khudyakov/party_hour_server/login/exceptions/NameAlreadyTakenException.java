package ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions;

public class NameAlreadyTakenException extends RuntimeException {
    public NameAlreadyTakenException(String userName) {
        super("Name " + userName + " is already taken");
    }
}
