package ru.nsu.ccfit.khudyakov.party_hour_server.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyFieldsException;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.EmptyHeadersException;
import ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions.UnknownStateError;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.NameAlreadyTakenException;
import ru.nsu.ccfit.khudyakov.party_hour_server.login.exceptions.UserNotFoundException;

@ControllerAdvice
public class ErrorController {
    private static final String LOGGER_MESSAGE_FORMAT = "code: {} message: {}";

    private static final int UNKNOWN_ERROR = 4000;
    private static final int NAME_ALREADY_TAKEN = 4001;
    private static final int USER_NOT_FOUND = 4002;
    private static final int EMPTY_FIELDS = 4012;
    private static final int EMPTY_HEADERS = 4013;


    private static final Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @ExceptionHandler(NameAlreadyTakenException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage processNameException(Exception e) {
        logger.info(LOGGER_MESSAGE_FORMAT, NAME_ALREADY_TAKEN, e.getMessage());
        return new ErrorMessage(NAME_ALREADY_TAKEN, e.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage processUserNotFoundException(Exception e) {
        logger.info(LOGGER_MESSAGE_FORMAT, USER_NOT_FOUND, e.getMessage());
        return new ErrorMessage(USER_NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler(UnknownStateError.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage processUnknownStateException(Exception e) {
        logger.info(LOGGER_MESSAGE_FORMAT, UNKNOWN_ERROR, e.getMessage());
        return new ErrorMessage(UNKNOWN_ERROR, e.getMessage());
    }


    @ExceptionHandler(EmptyFieldsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage processEmptyFieldsException(Exception e) {
        logger.info(LOGGER_MESSAGE_FORMAT, EMPTY_FIELDS, e.getMessage());
        return new ErrorMessage(EMPTY_FIELDS, e.getMessage());
    }

    @ExceptionHandler(EmptyHeadersException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorMessage processEmptyHeadersException(Exception e) {
        logger.info(LOGGER_MESSAGE_FORMAT, EMPTY_HEADERS, e.getMessage());
        return new ErrorMessage(EMPTY_HEADERS, e.getMessage());
    }

    public static int getUnknownError() {
        return UNKNOWN_ERROR;
    }

    public static int getNameAlreadyTaken() {
        return NAME_ALREADY_TAKEN;
    }

    public static int getUserNotFound() {
        return USER_NOT_FOUND;
    }

    public static int getEmptyFields() {
        return EMPTY_FIELDS;
    }

    public static int getEmptyHeaders() {
        return EMPTY_HEADERS;
    }
}