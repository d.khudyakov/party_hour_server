package ru.nsu.ccfit.khudyakov.party_hour_server.errors.general_exceptions;

public class EmptyHeadersException extends RuntimeException {
    public EmptyHeadersException(String message) {
        super(message);
    }
}
