package ru.nsu.ccfit.khudyakov.party_hour_server.plans;

import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.RegularPlanTO;
import ru.nsu.ccfit.khudyakov.party_hour_server.plans.model.UnitPlanTO;

import java.util.List;
import java.util.UUID;

public interface PlansService {
    void addUnitPlan(UnitPlanTO unitPlan, UUID userID);

    void addRegularPlan(RegularPlanTO regularPlan, UUID userID);

    List<RegularPlanTO> getRegularPlans(UUID userID);

    List<UnitPlanTO> getUnitPlans(UUID userID);
}
